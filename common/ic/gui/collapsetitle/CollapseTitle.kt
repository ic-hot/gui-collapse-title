package ic.gui.collapsetitle


import ic.struct.list.List

import ic.graphics.color.Color
import ic.graphics.color.ColorArgb
import ic.graphics.color.ext.toArgb

import ic.gui.align.Top
import ic.gui.scope.GuiScope
import ic.gui.scope.ext.views.space.Space
import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.Dp
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.ext.views.column.Column
import ic.gui.scope.ext.views.material.Material
import ic.gui.scope.ext.views.rect.Rect
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.view.View


inline fun GuiScope.CollapseTitle (

	titleBackgroundColor : Color,

	contentBackgroundColor : Color = Color.White,

	contentSheetCornersRadius : Dp,

	createTitle : GuiScope.() -> View,

	createHideableView : GuiScope.() -> View = { Space(height = 0.dp) },

	createContent : GuiScope.() -> View

) : View {

	val titleContent = createTitle()

	val title = Stack(
		width = ByContainer, height = ByContent,
		verticalAlign = Top,
		children = List(
			Material(
				child = Rect(color = titleBackgroundColor)
			),
			titleContent
		)
	)

	return Stack(
		width = ByContainer, height = ByContainer,
		children = List(
			Column(
				width = ByContainer, height = ByContainer,
				children = List(
					Space(height = titleContent.heightDp),
					Material(
						child = Stack(
							width = ByContainer, height = ByContainer,
							children = List(
								Rect(color = contentBackgroundColor),
								createContent()
							)
						)
					)
				)
			),
			title
		)
	)

}